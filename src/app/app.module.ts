import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { EmpListComponent } from './emp-list/emp-list.component';
import { EmployeeService } from './services/employee.service';
import { PaginationService } from './services/pagination.service';
import { PaginationComponent } from './shared/pagination/pagination.component';

/**
 * Root Module for the entire application
 */
@NgModule({
  declarations: [
    AppComponent,
    EmpListComponent,
    PaginationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [EmployeeService, PaginationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
