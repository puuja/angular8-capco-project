import { Component, Output, EventEmitter, Input, OnChanges } from '@angular/core';
import { PaginationService } from '../../services/pagination.service';
import { Employee } from '../../model/employee';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnChanges {

  //pager object 
  pager: any = {};

  //page size
  pageSize: number = 5;

  //paged items 
  pagedItems: any[];
  
  //array of Employee
  @Input() public employees: Employee[];

  //custom event to emit the paged employees
  @Output() customEvent = new EventEmitter();

  /**
   * Parameterized Constructor
   * @param paginationService : Pagination Service object
   */
  constructor(private paginationService: PaginationService) { }

  ngOnChanges() {
    this.selectItemsToBePaged(5);
  }

  /**
   * This method sets the paged items based on the input page number
   * @param page : holds the current page number
   */
  setPage(page: number) {
    // get pager object from service
    this.pager = this.paginationService.getPager(this.employees.length, page, this.pageSize);
    // get current page of items
    this.pagedItems = this.employees.slice(this.pager.startIndex, this.pager.endIndex + 1);
    //emit all the pagedItems
    this.customEvent.emit(this.pagedItems);
  }

  /**
   * This method selects all the items that is required to be paged
   * @param itemCount : number of items in a single page
   */
  selectItemsToBePaged(itemCount: number) {
    this.pageSize = itemCount;
    this.setPage(1);
  }
}
