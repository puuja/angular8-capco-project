import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Employee } from '../model/employee';

/**
 * Component to fetch employee list data
 */
@Component({
  selector: 'app-emp-list',
  templateUrl: './emp-list.component.html',
  styleUrls: ['./emp-list.component.css']
})
export class EmpListComponent implements OnInit, AfterViewInit {
  
  //paged items 
  pagedItems: any[];

  //array of Employee
  public employees: Employee[];

  //custom observer
  observer = {
    next: data => {
      this.employees = data
    },
    error: err=>console.log('Error ', err),
    complete: ()=>console.log('Data fetched successfully from sample-data.json')  
  };

  /**
   * Injects the EmployeeService for Injection
   * 
   * @param employeeService : EmployeeService : Injects the EmployeeService to makes the call to available method.
   */
  constructor(private employeeService: EmployeeService, private cdr: ChangeDetectorRef) { }

  /**
   * On initialization life cycle hook
   */            
  ngOnInit() {
    // this will helps to get the list of employee to be loaded at start
    this.employeeService.loadEmployees().subscribe(this.observer);
  }

  /**
   * After View initialization life cycle hook
   */
  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  /**
   * Method to submit employee status through service
   * @param employee : Contains the entire employee data within a single row
   */
  public submitStatus(employee) {
    this.employeeService.submitEmployeeStatus(employee);
  }

  /**
   * This method gets items to be shown in view
   * @param pagedItems Items to be paged
   */
  itemsToBePaged(pagedItems) {
    this.pagedItems = pagedItems;
  }
}