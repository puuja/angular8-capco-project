export class Employee {
    name: String;
    phone: String;
    email: String;
    company: String;
    date_entry: String;
    org_num: String;
    address_1: String;
    city: String;
    zip: String;
    geo: String;
    pan: String;
    pin: String;
    id: number;
    status: String;
    fee: String;
    guid: String;
    date_exit: String;
    date_first: String;
    date_recent: String;
    url: String;
}
