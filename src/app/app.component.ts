import { Component } from '@angular/core';

/**
 * Root component for the application
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Capco-Angular-Task';
}
