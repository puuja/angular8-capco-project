## Project Description 

This is Angular project with version 8.0.3 which has the following implementation points :

1. The component should display Sample Data in a table
2. User should be able to select how many rows are displayed in the table
3. Table should be paginated if not all rows are displayed on the screen based on the user�s selection
4. Pagination options should be displayed in the table footer
5. Column names should be displayed in the table header
6. Entire table, table header and table footer should always be displayed on the screen while scrolling
7. If number of rows exceeds the size of the table body, a vertical scrollbar should be displayed within the table body � only table body shall scroll vertically, table header and footer shall remain as is
8. If number of columns exceed the size of the table body, a horizontal scrollbar should be displayed within the table    body � only table body and table header shall scroll to reveal the additional columns, table footer shall remain as is
9. Each row should contain a button which shall submit the row ID and row status to /api/submit as a POST request � You are not expected to create the POST endpoint, but you can mock one if you like

---

## Technologies:

. Angular (version 8.0.3)
. Angular CLI (version 8.0.6)

---

## Clone a repository

git clone https://puuja@bitbucket.org/puuja/angular8-capco-project.git
---

# CapcoAngularTask

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

# Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).